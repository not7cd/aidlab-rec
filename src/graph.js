class Graph {
  constructor(id, width, height, interpolation, animate, updateDelay, transitionDelay, device) {
    this._device = device

    this.graph = d3.select(id).append("svg:svg").attr("width", "100%").attr("height", "100%");

    this.data = [];
    // X scale will fit values from 0-10 within pixels 0-100
    this._x = d3.scaleLinear().domain([0, 48]).range([-5, width]); // starting point is -5 so the first value doesn't show and slides off the edge as part of the transition
    // Y scale will fit values from 0-10 within pixels 0-100
    this._y = d3.scaleLinear().domain([0, 10]).range([0, height]);

    // create a line object that represents the SVN line we're creating
    this.line = d3.line()
      .x((d, i) => this._x(i))
      .y((d) => this._y(d));
    if(interpolation == "basis") {
      this.line.curve(d3.curveCatmullRom.alpha(0.5))
    }

    this.graph.append("svg:path").attr("d", this.line(this.data));

    this._transitionDelay = transitionDelay;
    1
    this._intervalID = setInterval(() => {
      if(this.data.length > 48) {
        this.data.shift();
      }
      this.data.push(this._device.getMeasurment());

      if(animate && this.data.length > 48) {
        this._redrawWithAnimation();
      } else {
        this._redrawWithoutAnimation();
      }
    }, updateDelay);
  }

  _redrawWithAnimation() {
    // update with animation
    this.graph.selectAll("path")
      .data([this.data]) // set the new data
      .attr("transform", "translate(" + this._x(1) + ")") // set the transform to the right by x(1) pixels (6 for the scale we've set) to hide the new value
      .attr("d", this.line) // apply the new data values ... but the new value is hidden at this point off the right of the canvas
      .transition() // start a transition to bring the new value into view
      .ease(d3.easeLinear)
      .duration(this._transitionDelay * 0.95) // for this demo we want a continual slide so set this to the same as the setInterval amount below
      .attr("transform", "translate(" + this._x(0) + ")"); // animate a slide to the left back to x(0) pixels to reveal the new value

    /* thanks to 'barrym' for examples of transform: https://gist.github.com/1137131 */
  }

  _redrawWithoutAnimation() {
    // static update without animation
    this.graph.selectAll("path")
      .data([this.data]) // set the new data
      .attr("d", this.line); // apply the new data values
  }

}
