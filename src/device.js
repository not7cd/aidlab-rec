class AidlabDevice {
  constructor() {
    // TODO: gen real id
    this.id = Math.floor((Math.random() * 100) + 1)
    this.device = null;
    this.server = null;
    this._characteristics = new Map();
    this._data = [0];
    this.pulse = 0;

  }

  connect() {
    console.log("Connecting to device");

    return navigator.bluetooth.requestDevice({
        acceptAllDevices: true,
        optionalServices: ['44366e80-cf3a-11e1-9ab4-0002a5d5c51b']
      })
      .then(device => this.connectToDevice(device))
      .then(server => {
        console.log("Get characteristics");
        this.server = server;
        return Promise.all([
          server.getPrimaryService('44366e80-cf3a-11e1-9ab4-0002a5d5c51b').then(service => {
            return Promise.all([
              this._cacheCharacteristic(service, '46366e80-cf3a-11e1-9ab4-0002a5d5c51b'), // ECG
              this._cacheCharacteristic(service, '47366e80-cf3a-11e1-9ab4-0002a5d5c51b'), // BATTERY
            ])
          })
        ]);
      })
      .then(_ => {
        return this.startNotificationsEcgRateMeasurement()
          .then(characteristic => {
            console.log('> Notifications started');
            characteristic.addEventListener('characteristicvaluechanged',
              this.handleEcgRateChanged.bind(this));
          })
      }).then(_ => {
        console.log("Connected");
        return this
      })
      .catch(error => {
        console.log('Argh! ' + error);
      });
  }

  connectToDevice(device) {
    console.log('Connecting to Bluetooth Device...');
    this.device = device;
    return this.device.gatt.connect()
  }

  getMeasurment() {
    console.log('Returning measurment:' + (this.pulse / 10));
    return parseInt(this.pulse / 10)
  }



  handleEcgRateChanged(event) {
    let rate = event.target.value.getFloat32(0);
    // console.log(event.target.value.getUTF8String());
    this.pulse = 50;
    // console.log('> Ecg Rate is ' + this.pulse + '');
    // this._data.push(rate);
  }

  startNotificationsEcgRateMeasurement() {
    return this._startNotifications('46366e80-cf3a-11e1-9ab4-0002a5d5c51b');
  }
  stopNotificationsEcgRateMeasurement() {
    return this._stopNotifications('46366e80-cf3a-11e1-9ab4-0002a5d5c51b');
  }


  _cacheCharacteristic(service, characteristicUuid) {
    return service.getCharacteristic(characteristicUuid)
      .then(characteristic => {
        this._characteristics.set(characteristicUuid, characteristic);
      });
  }
  _readCharacteristicValue(characteristicUuid) {
    let characteristic = this._characteristics.get(characteristicUuid);
    // console.log(characteristic);
    return characteristic.readValue()
      .then(value => {
        // In Chrome 50+, a DataView is returned instead of an ArrayBuffer.
        console.log(value);
        value = value.buffer ? value : new DataView(value);
        // value = value.getUint8(0)
        return value;
      });
  }
  _writeCharacteristicValue(characteristicUuid, value) {
    let characteristic = this._characteristics.get(characteristicUuid);
    return characteristic.writeValue(value);
  }
  _startNotifications(characteristicUuid) {
    console.log('Start notifications for ' + characteristicUuid);
    let characteristic = this._characteristics.get(characteristicUuid);
    // Returns characteristic to set up characteristicvaluechanged event
    // handlers in the resolved promise.
    return characteristic.startNotifications()
      .then(() => characteristic);
  }
  _stopNotifications(characteristicUuid) {
    let characteristic = this._characteristics.get(characteristicUuid);
    // Returns characteristic to remove characteristicvaluechanged event
    // handlers in the resolved promise.
    return characteristic.stopNotifications()
      .then(() => characteristic);
  }

}

// exports.AidlabDevice = AidlabDevice;
