Vue.component('device-item', {
  props: ['device'],
  template: '<div><p>Aidlab {{ device.id }}</p><p>Pulse {{ device.pulse }} <div v-bind:id="\'graph-\' + device.id" class="graph aGraph" style="width:300px; height:60px;"></div></div>',
})

let app = new Vue({
  el: '#app',
  data: {
    title: "Diagnostics",
    devices: []
  },
  methods: {
    addDevice: function() {
      new Promise(function(resolve, reject) {
        resolve(new AidlabDevice().connect())
      }).then(device => {
        console.log(device);
        app.devices.push(device);
        return device
      }).then(device => {
        device.graph = new Graph("#graph-" + device.id, 300, 60, "basis", true, 100, 100, device);
      }).catch(error => {
        console.log('Argh! ' + error);
      });
    }
  }
})
