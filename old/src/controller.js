function toUUID(uuid) {
  return uuid.toLowerCase()
}

const SERVICESUUID = {
  userService: toUUID("44366E80-CF3A-11E1-9AB4-0002A5D5C51B"),
  deviceInformationService: toUUID("180A")
}
const CHARACTERISTICSUUID = {
  ecg: toUUID("46366E80-CF3A-11E1-9AB4-0002A5D5C51B"),
  battery: toUUID("47366E80-CF3A-11E1-9AB4-0002A5D5C51B"),
  serialNumber: toUUID("2A25")
}

let aidlabDevice
let aidlabCharacteristics = {}
let data = [1, 2, 3, 4, 5, 6, 7, 8, 9]
let chart


function onClickAddDevice() {
  return(aidlabDevice ? Promise.resolve() : requestDevice())
    .then(connectDeviceAndCacheCharacteristics)
    .then(_ => {
      console.log('Reading Battery Level...');
      return aidlabCharacteristics['ecg'].readValue();
    })
    .catch(error => {
      console.log('Argh! ' + error);
    });
}

function requestDevice() {
  service = SERVICESUUID['userService'];
  console.log('Requesting any Bluetooth Device with', service);
  return navigator.bluetooth.requestDevice({
      filters: [{
        services: [service]
      }]
    })
    .then(device => {
      aidlabDevice = device;
      aidlabDevice.addEventListener('gattserverdisconnected', onDisconnected);
    });
}

function connectDeviceAndCacheCharacteristics() {
  if(aidlabDevice.gatt.connected && batteryLevelCharacteristic) {
    return Promise.resolve();
  }

  console.log('Connecting to GATT Server...');
  return aidlabDevice.gatt.connect()
    .then(server => {
      console.log('Getting Battery Service...');
      return server.getPrimaryService(SERVICESUUID['userService']);
    })
    .then(service => {
      console.log('Getting Battery Level Characteristic...');
      characteristic = {}
      characteristic['ecg'] = service.getCharacteristic(CHARACTERISTICSUUID['ecg'])
      characteristic['battery'] = service.getCharacteristic(CHARACTERISTICSUUID['battery'])
      return characteristic;
    })
    .then(characteristic => {
      ecgCharacteristic = characteristic['ecg'];
      ecgCharacteristic.addEventListener('characteristicvaluechanged',
        handleEcgChanged);
    });
}

function handleEcgChanged(event) {
  let ecgValue = event.target.value.getUint8(0);
  console.log('> ecgValue is ' + ecgValue + '%');
  document.querySelector('#raw-data').innerHTML += ecgValue + "</br>"
  updateChart();
}



function onDisconnected() {
  console.log('> Bluetooth Device disconnected');
  connectDeviceAndCacheCharacteristics()
    .catch(error => {
      console.log('Argh! ' + error);
    });
}

function updateChart() {
  var _data = {
    // A labels array that can contain any sort of values
    labels: data,
    // Our series array that contains series objects or in this case series _data arrays
    series: [
      [data]
    ]
  };
  // naive updating
  chart = new Chartist.Line('#line-chart', _data);
}

updateChart()
